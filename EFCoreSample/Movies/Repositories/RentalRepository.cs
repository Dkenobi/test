using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreSample.Movies.Data;
using EFCoreSample.Movies.Domain;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore;

namespace EFCoreSample.Movies.Repositories
{
    public class RentalRepository : IRentalRepository
    {
        private readonly AppDatabaseContext _db;

        public RentalRepository(AppDatabaseContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<MovieRental>> GetAll()
        {
            var dataModel = await _db.Rental
                .Include(rental => rental.Movies)
                .ToListAsync();
            return dataModel.Select(ToDomainModel);
        }

        public async Task<MovieRental> Get(Guid id)
        {
            var success = await _db.Rental.Include(e => e.Movies)
                .FirstOrDefaultAsync(entity => entity.Id == id);

            return success == null ? null : ToDomainModel(success);
        }

        public async Task<MovieRental> Add(MovieRentalValue value)
        {
            if (value == null) return null;
            var rental = new MovieRental(Guid.NewGuid(), value);

            await _db.Rental.AddAsync(ToRentalDataModel(rental));
            await _db.SaveChangesAsync();
            return await Get(rental.Id);
        }

        public Task<MovieRental> AddMovieToRental(Guid id, Movie movie)
        {
//            if (!_db.FindAsync(id)) return null;
//            
//            var movieData = new MovieDataModel
//            {
//                Id = movie.Id,
//                Title = movie.Value.Title,
//                DirectorName = movie.Value.DirectorName,
//                ReleaseDate = movie.Value.ReleaseDate
//            };
//
//            var rental = _db[id];
//            rental.Movies.Add(movieData);
//            _db[id] = rental;
            return null;
        }

        public async Task<MovieRental> Update(Guid id, MovieRentalValue value)
        {
            var dataModel = await _db.Rental.Include(e => e.Movies)
                .FirstOrDefaultAsync(entity => entity.Id == id);

            if (dataModel == null) return null;

            dataModel.Name = value.Name;
            dataModel.Address = value.Address;

            await _db.SaveChangesAsync();

            return ToDomainModel(dataModel);
        }

        public async Task<bool> Delete(Guid id)
        {
            var toBeDeleted = await _db.Rental.Include(e => e.Movies)
                .FirstOrDefaultAsync(entity => entity.Id == id);

            if (toBeDeleted == null) return false;

            _db.Remove(toBeDeleted);

            await _db.SaveChangesAsync();
            return true;
        }

        private MovieRental ToDomainModel(RentalDataModel dataModel)
        {
            return new MovieRental(dataModel.Id, new MovieRentalValue(dataModel.Name, dataModel.Address));
        }

        private RentalDataModel ToRentalDataModel(MovieRental rental)
        {
            return new RentalDataModel
            {
                Id = rental.Id,
                Name = rental.Value.Name,
                Address = rental.Value.Address,
                Movies = rental.Movies?.Select(ToMovieDataModel).ToList()
            };
        }

        private MovieDataModel ToMovieDataModel(Movie movie)
        {
            return new MovieDataModel
            {
                Id = movie.Id,
                Title = movie.Value.Title,
                DirectorName = movie.Value.DirectorName,
                ReleaseDate = movie.Value.ReleaseDate
            };
        }
    }
}